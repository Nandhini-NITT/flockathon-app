var flock = require('flockos');
var config = require('./config.js');
var express = require('express');
var fs = require('fs');
var mysql = require("mysql");
var moment = require('moment');
var app = express();
var cheerio = require('cheerio');
var request = require('request');
var async=require("async");
var groupArray = require('group-array');
app.locals.moment = moment; // this makes moment available as a variable in every EJS page
flock.setAppId(config.appId);
flock.setAppSecret(config.appSecret);
var cron = require('node-schedule');
app.set('view engine','ejs');
//Listen for events on /events, and verify event tokens using the token verifier.
app.use(flock.events.tokenVerifier);
app.post('/events', flock.events.listener);
var path = require('path');
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "assistant"
});

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
con.connect(function(err){
  if(err){
    console.log('Error connecting to Db');
    return;
  }
  console.log('Connection established');
});

function checkTime(){
  var date=new Date();
  var event_id,participant_id,location,createdBy,venue_id,createdBy_name;
    con.query('Select * from events',function(err,rows){
      if(err) throw err;
      if(rows.length>0)
      {
        for(var i=0;i<rows.length;i++)
      {   
          if(moment(rows[i].time).diff(moment(),'minutes')==60)
          {
            event_id=rows[i].event_id;
          location=rows[i].location;
          createdBy=rows[i].createdBy;
          createdBy_name=rows[i].createdBy_name;
          event_type=rows[i].event_type;
          venue_id=rows[i].venue_id;
          con.query('select * from event_participants where event_id=?',event_id,function(err,res){
            console.log(err);
            for(var j=0;j<res.length;j++)
            {
              participant_id=res[j].participant_id;
              flock.callMethod('chat.sendMessage',config.botToken,{
                to:participant_id,
                attachments:[{
              title: "Invitation",
              description: "Event Invitation",
              views: {
                flockml:'<flockml>This is to remind you that the event '+event_type+' at <a href="https://foursquare.com/v/'+venue_id+'" target="_blank">'+location+'</a> for which you were invited by <user userId="'+createdBy+'">'+createdBy_name+'</user> will take place in an hour. '
                },
                buttons:[{
                  name:'View',
                  action:{type:'openWidget',desktopType:'sidebar',mobileType:'sidebar',url:'https://0e4ada1d https://d95df81a.ngrok.io/eventBar'},
                  id:'view',

                }]
              }]
                
    }
                ,function(err,response){
              if(err) console.log(err);
            });
          }
        });
      }
    }
    }
});
  }
checkTime();
var cronJob = cron.scheduleJob("00 *  * * *", function(){
     checkTime();
    
}); 


app.get('/scrape',function(req,res){
  var city=req.query.city;
  var link,movies=[],title;
  url="https://www.ticketnew.com/"+city+'/movies';
  console.log(url);
  request(url, function(error, response, html){

   if(!error && response.statusCode==200){
    var $ = cheerio.load(html);
    var mainClass=$('.theatre_sections');
    for(var i=0;i<mainClass.length;i++)
    {
      console.log(mainClass.length);
     link=mainClass.eq(i).first().find('a').attr('href')+'/C/chennai';
     link=link.replace('Release-Date','Online-Advance-Booking')
     movies.push({
        link: link,
      });
      
    }
    console.log(movies);
    res.send(movies);
    }
  
  else
        throw error;
    });
});
app.get('/download.png',function(req,res){
  res.sendFile(path.join(__dirname + '/download.png'));
});
app.post('/scrapeImage', function (req, res) {
  "use strict";
    let urls = [],movies=[];
    var links=req.body,counter=0;
    for (let y = 0; y < links.length; y++) {
        urls.push(links[y].link);
    }
    
    function httpGet(url, callback) {
  const options = {
    url :  url,
    json : true
  };
  request(options,
    function(err, res, html) {
      if(html){
      var $$=cheerio.load(html);
        let status="";
        let mainId=$$('#divMoviedetails');
        let img=$$('.mov_img_b').find('img').attr('src') || $$('.movie-info-image').find('img').attr('src');
        let movieDetails=mainId.eq(1);
        let title=$$('span[itemprop="name"]').attr('title')|| $$('.movie_info_b_l_info h3').text();
        let language=$$('span[itemprop="inLanguage"]').html()|| $$('.sub_til >ul>li').first().text();
        if($$('#movies-date-container').length>0)
          status='Now showing';
        else
          status='Coming soon';
        movies.push({
          title:title,
          img:img,
          language:language,
          status:status,
          url:url
        }); 
      callback(err, movies);
    }
  }
  );
}
async.map(urls, httpGet, function (err, res1){
  if (err) return console.log(err);
  console.log(movies.length);
  res.send(movies);
});
});
app.get('/movies',function(req,res){
    var token = req.get('x-flock-event-token') || req.query.flockEventToken;
        if (token) {
            var payload = flock.events.verifyToken(token);
            if (!payload) {
                console.log('Invalid event token', token);
                res.sendStatus(403);
                return;
            }
            res.locals.eventTokenPayload = payload;
            res.render(path.join(__dirname+'/movies'),{userId:payload.userId,siteAddress:config.siteAddress});
}
});
app.get('/getContacts',function(req,res){
  var userId=req.query.userId;
  console.log(userId);
  con.query('SELECT token from userToken where userId=?', userId, function(err,res1){
    if(!err)
    {
      if(res1.length>0)
      {
      var token=res1['0'].token;
      flock.callMethod('roster.listContacts',token,{},function(error,response){
       if(!error)
        res.send(response);
      else
        console.log(error);
    }); 
    }
      else
        res.send('Not a user');
  }
      });
    });

app.get('/delete',function(req,res){
  console.log('inside Delete');
  var event_id=req.query.event_id;
  var user_id=req.query.user_id;
  var username=req.query.username;
  var date,location,venue_id,createdBy,event_type;
  var token = req.get('x-flock-event-token') || req.query.flockEventToken;
    con.query('Select * from events where event_id=?',event_id,function(error,response){
    if(error) console.log(error);
    else
    {
      if(response.length==0)
        res.send("Event Doesn't exist");
      console.log(response);
      event_type=response[0].event_type;
      location=response[0].location;
      venue_id=response[0].venue_id;
      date=response.time;
      date=moment(date).format("dddd, MMMM Do YYYY, h:mm:ss a");
      username=response[0].createdBy_name;
      createdBy=response[0].createdBy;
      if(createdBy===user_id)
        {
          console.log('delete admin');
          con.query('select participant_id from event_participants where event_id=?',event_id,function(err,response){
            if(err) console.log(err);
            else
            {
              console.log(response);
              for(var i=0;i<response.length;i++)
              {
                flock.callMethod('chat.sendMessage',config.botToken,{
                to:response[i].participant_id,
                //text: 'You have been invited for '+event_type+' at '+location+' by '+username,
                 attachments:[{
                 title: "Cancellation of Event",
                 description: "Event cancellation",
                  views: {
                    flockml:'<flockml>This is to inform you that the event plan: '+event_type+' at <a href="https://foursquare.com/v/'+venue_id+'" target="_blank">'+location+'</a> created by <user userId="'+user_id+'">'+username+'</user> on '+date+' is cancelled due to some unavoidable reasons.'
                    },
                  buttons:[{
                  name:'View',
                  action:{type:'openWidget',desktopType:'sidebar',mobileType:'sidebar',url:'https://451bae8d https://d95df81a.ngrok.io/eventBar'},
                  id:'view',
                  }]
              }]
  },function(error,response)
    {
      if(!error)
        console.log(response);
      else
        console.log(error);
    });
              }
            con.query('delete from events where event_id=?',event_id,function(error,res2){
              if(error) console.log(error);
              else
              {
                con.query('select *  from event_participants P right join events E on P.event_id=E.event_id',function(err,res1){
                if(!err)
                {
                  res1=groupArray(res1,'event_id');
                  res1=JSON.stringify(res1);
                  res.render(path.join(__dirname + '/event_organizer'),{userId:user_id,username:username,rows:res1,deleted:'success'});
                 }
              });
              }
            });
            }
          })
        }
        else
        {
          con.query('select * from event_participants where participant_id=?,event_id=?',[user_id,event_id],function(error,responseCheck){
            if(error) console.log(error);
            else
            {
              if(responseCheck.length==0)
                ress.send('Could not complete action');
            }
          });
          flock.callMethod('chat.sendMessage',config.botToken,{
                to:createdBy,
                //text: 'You have been invited for '+event_type+' at '+location+' by '+username,
               attachments:[{
              title: "Decline event invitation",
              description: "",
              views: {
                flockml:'<flockml>This is to inform you that <user userId="'+user_id+'">'+username+'</user> will not be able to attend '+event_type+' at <a href="https://foursquare.com/v/'+venue_id+'" target="_blank">'+location+'</a> created by you on '+date+' due to some unavoidable reasons.'
                },
                buttons:[{
                  name:'View',
                  action:{type:'openWidget',desktopType:'sidebar',mobileType:'sidebar',url:'https://0e4ada1d https://d95df81a.ngrok.io/eventBar'},
                  id:'view',

                }]
              }]
                
    },function(error,response)
    {
      if(!error)
        {
          con.query('delete from event_participants where participant_id=?',user_id,function(err,resD){
            if(err) console.log(err);
            else
            {
              con.query('select *  from event_participants P right join events E on P.event_id=E.event_id',function(err,res1){
                if(!err)
                {
                  res1=groupArray(res1,'event_id');
                  res1=JSON.stringify(res1);
                  res.send('Event was deleted successfully');
                 }
              });
              
            }
          });
        }
      else
        console.log(error);
    });
        }
    }
  });

  });
app.get('/icon',function(req,res){
  res.sendFile(path.join(__dirname + '/icon.png'))
})
app.post('/addEvent',function(req,res){
  var username='';
  var event_type=req.body.eventType;
  var participants=JSON.parse(req.body.participants_id);
  var date=req.body.date;
  var venue_id=req.body.venue_id;
  var location=req.body.location;
  var userId=req.body.userId;
  var participant,token;
  var username=req.body.username;
  var events={event_type:event_type,location:location,venue_id:venue_id,createdBy:userId,time:date,createdBy_name:username}
  con.query('INSERT into events set ?',events,function(error,response){
    if(!error)
      {
        console.log('event added');
        var event_id=response.insertId;
        participants=participants.map(function(val){
          participant={event_id:event_id,participant_id:val.participant_id,participant_name:val.participant_name};
          con.query('INSERT into event_participants set ?',participant,function(error,response)
          {
        if(!error) 
            {
              date=moment(date).format("dddd, MMMM Do YYYY, h:mm:ss a");
              flock.callMethod('chat.sendMessage',config.botToken,
              {
                to:val.participant_id,
                //text: 'You have been invited for '+event_type+' at '+location+' by '+username,
               attachments:[{
              title: "Invitation",
              description: "",
              views: {
                flockml:'<flockml>You have been invited for '+event_type+' at <a href="https://foursquare.com/v/'+venue_id+'" target="_blank">'+location+'</a> by <user userId="'+userId+'">'+username+'</user> on '+date
                },
                buttons:[{
                  name:'View',
                  action:{type:'openWidget',desktopType:'sidebar',mobileType:'sidebar',url:'https://9b8cd9e0 https://d95df81a.ngrok.io/eventBar'},
                  id:'view',
                  },
                  {
                    name:'Decline',
                    action:{type:'openWidget',desktopType:'modal',mobileType:'modal',url:'https://451bae8d https://d95df81a.ngrok.io/delete?event_id='+event_id+'&user_id='+val.participant_id+'&username='+username},
                    id:'decline',
                    icon:'https://451bae8d https://d95df81a.ngrok.io/icon'
                  }]
              }]
                
    },function(error,response)
    {
      if(!error)
        console.log(response);
      else
        console.log(error);
    });
   con.query('select *  from event_participants P right join events E on P.event_id=E.event_id',function(err,res1){
              if(!err)
              {
                res1=groupArray(res1,'event_id');
                res1=JSON.stringify(res1);
                res.render(path.join(__dirname + '/event_organizer'),{userId:userId,username:username,rows:res1,config:config});
              }
            });
            
            }
            else console.log(error);
          });

        });
      }
});
});

var port = config.port || 8080;
app.listen(port, function () {
    console.log('Listening on port: ' + port);
});
  /*app.get('/food',function(req,res){
  var token = req.get('x-flock-event-token') || req.query.flockEventToken;
        if (token) {
            var payload = flock.events.verifyToken(token);
            if (!payload) {
                console.log('Invalid event token', token);
                res.sendStatus(403);
                return;
            }
            res.locals.eventTokenPayload = payload;
            res.render(path.join(__dirname+'/food'),{userId:payload.userId});
          }});
flock.events.on('app.install', function (event) {
    var user = { userId: event.userId, token: event.token };
    con.query('INSERT INTO userToken SET ?', user, function(err,res){
  if(!err)
  {
    console.log('user registered successfully');
    flock.callMethod('chat.sendMessage', config.botToken, {
    to: event.userId,
    text: "Thank you for installing our app"
}, function (error, response) {
    if (!error) {
        console.log(response);
    }
});
    
  }
});
});*/
flock.events.on('app.uninstall', function (event) {
    con.query(
      'DELETE FROM userToken WHERE userId = ?',
      event.userId,
  function (err, result) {
    if (err) throw err;
  }
);
});
/*flock.events.on('chat.receiveMessage',function(event){
  console.log(event.message.text)
  if(event.message.text=='/movies')
    flock.callMethod('chat.sendMessage',config.botToken,
    {
      to:event.userId,
      text:'A message with an attachment',
      attachments:[{
        title: "Movies near you",
        description: "Now showing",
        views: {
          widget:{src:config.siteAddress+'/movies',height:300,width:300}
        }
      }]
    },function(error,response)
    {
      if(!error)
        console.log(response);
      else
        console.log(error);
    });
  else if(event.message.text==='food')
    flock.callMethod('chat.sendMessage',config.botToken,
    {
      to:event.userId,
      text:'Food',
      attachments:[{
        title: "Foodie spots near you",
        description: "List of Restaurants based on your location",
        views: {
          widget:{src:config.siteAddress+'/food',height:400}
        }
      }]
    },function(error,response)
    {
      if(!error)
        console.log(response);
      else
        console.log(error);
    });
});*/


app.get('/upvote',function(req,res){
  var userId=req.query.userId;
  var username=req.query.username;
  var comment_id=req.query.comment_id;
  con.query('update comments set upvotes=upvotes+1 where comment_id=?',comment_id,function(error,response){
    if(error) console.log(error);
    else{
      var upvote={comment_id:comment_id,upvoteBy:userId,upvoteBy_name:username};
      con.query('insert into upvotes set ?',upvote,function(err,response){
        if(err) console.log(err);
        else
        {
          res.send('done');
        }
      })
    }
  })
});
app.get('/getDiscussion',function(req,res){
  var username=JSON.parse(req.query.flockEvent).userName;
  var userId=JSON.parse(req.query.flockEvent).userId;
  var comments=req.body.comments;
  console.log('in getDiscussion');
  con.query('select D.discussion_id,D.discussion_name  from discussion_participants P right join discussions D on P.discussion_id=D.discussion_id where D.createdBy=? or P.participant_id=?',[userId,userId],function(error,userDiscussions)
  {
    if(!error) 
    {
        //res.send(userDiscussions);
        res.render(path.join(__dirname + '/selectDiscussion'),{username:username,userId:userId,userDiscussions:userDiscussions,comments:comments});
    }
  });
});
app.post('/addComment',function(req,res){
  var givenBy=req.body.userId;
  var discussion_id=req.body.discussion_id;
  var comment=req.body.comment||req.body.comment;
  console.log(comment);
  var username=req.body.username;
  var comment={givenBy:givenBy,discussion_id:discussion_id,comment:comment};
  con.query('insert into comments set ?',comment,function(error,response){
    if(error) console.log(error);
    else
    {
      con.query('select *  from event_participants P right join events E on P.event_id=E.event_id',function(err,res1){
              if(!err)
              {
                res1=groupArray(res1,'event_id');
                res1=JSON.stringify(res1);
                con.query('select *  from discussion_participants P right join discussions D on P.discussion_id=D.discussion_id where D.createdBy=? or P.participant_id=?',[givenBy,givenBy],function(error,userDiscussions)
  {
    if(!error) 
    {
             async.map(userDiscussions,findcomments,function(err,results){
            console.log('inside async');
            if(err) console.log(err);
            if(!err) 
            {
              console.log(results);
              comments=JSON.stringify(results);
              res.render(path.join(__dirname + '/event_organizer'),{username:username,userId:givenBy,latestcomments:comments,rows:res1});
            
              
            }
        });
    }
  });
              }
            });
    }
  });

});
var findcomments=function(val,fn){
  console.log('in find comments');
  con.query('select comments.comment,comments.comment_id,comments.upvotes,discussions.discussion_id,discussions.discussion_name,upvotes.upvoteBy from comments left join upvotes on comments.comment_id=upvotes.comment_id right join discussions on discussions.discussion_id=comments.discussion_id where discussions.discussion_id=?',val,function(error,results){
    if(error) console.log(error);
    else
      {
        console.log(results);
        return fn(null,results);
      }
  });
}
app.get('/eventBar',function(req,res){
var token = req.get('x-flock-event-token') || req.query.flockEventToken;
var userName="";
console.log(token);

        if (token) {
            var payload = flock.events.verifyToken(token);
            if (!payload) {
                console.log('Invalid event token', token);
                res.sendStatus(403);
                return;
            }
            res.locals.eventTokenPayload = payload;
            con.query('select *  from event_participants P right join events E on P.event_id=E.event_id',function(err,res1){
              if(err) console.log(err);
              if(!err)
              {
                res1=groupArray(res1,'event_id');
                res1=JSON.stringify(res1);
                console.log(res1);
                userName=JSON.parse(req.query.flockEvent).userName;
                con.query('select D.discussion_id  from discussion_participants P right join discussions D on P.discussion_id=D.discussion_id where D.createdBy=? or P.participant_id=?',[payload.userId,payload.userId],function(error,userDiscussions)
  {
    if(error) console.log(error);
    if(!error) 
    {
          console.log(userDiscussions);
          userDiscussions=userDiscussions.map(item => item.discussion_id)
  .filter((value, index, self) => self.indexOf(value) === index)
          console.log(userDiscussions);
          async.map(userDiscussions,findcomments,function(err,results){
            console.log('inside async');
            if(err) console.log(err);
            if(!err) 
            {
              console.log(results);

              comments=JSON.stringify(results);
              res.render(path.join(__dirname + '/event_organizer'),{username:userName,userId:payload.userId,latestcomments:comments,rows:res1});
            
              
            }
        });
      
    }
  });
              }
            });
            
          }
});
 var insertParticipants=function(discussion_id,discussion_name,userId,username){
  return function(val,fn)
{
  console.log(val);
  var participant={discussion_id:discussion_id,participant_id:val.participant_id,participant_name:val.participant_name};
        con.query('insert into discussion_participants set ?',participant,function(error,response){
          
          if(!error)
          {
           
        flock.callMethod('chat.sendMessage',config.botToken,{
          to:val.participant_id,
          attachments:[{
            title:"Discussion Channel",
            description:"New Discusssion channel created",
            views:[{
              flockml:"<flockml><user userId='"+userId+"'>"+username+"</user> has created a new discussion: "+discussion_name,            }]
          }],
          buttons:[{
             name:'View',
                  action:{type:'openWidget',desktopType:'sidebar',mobileType:'sidebar',url:'https://9b8cd9e0 https://d95df81a.ngrok.io/eventBar'},
                  id:'view',
            }]
         },function(error,response){
          if(!error)  fn(null,response);
          
         });
        //console.log(fn(response));
fn(null,'done');
      }
    });
      }
}
app.get('/added',function(req,res){
  res.render(path.join(__dirname + '/added'));
})
app.post('/newDiscussion',function(req,res){
  console.log(req.body)
  var discussion_name=req.body.discussion_name;
  var status='';
  var userId=req.body.userId;
  var username=req.body.username;
  var participants=JSON.parse(req.body.participants);
  console.log('discuss'+username);
  var discussion={discussion_name:discussion_name,createdBy:userId,createdBy_name:username};
  con.query('Insert into discussions set ?',discussion,function(error,response){
    if(error) {console.log(error);status='failure';}
    else
    {
      status='success';
      console.log(response)
      var discussion_id=response.insertId;
      console.log('participants');
      console.log(participants);
         async.map(participants,insertParticipants(discussion_id,discussion_name,userId,username),function(error,response){
      if(error) console.log(error);
      else
        {
          res.send('done');       
      
        } 
  });
        }
     
   });
 });
   
  var getColumns=function(val,doneCallBack)
  {
    var results= { participant_id: val.id, participant_name: val.firstName };
    doneCallBack(null,results);
  }

app.get('/showSuggestion',function(req,res){

  res.render(path.join(__dirname + '/discuss'));
})
flock.events.on('client.slashCommand',function(event){
  var command=event.command;
  var to=event.chat;
  var username=event.userName;;
  var userId=event.userId;
  var text=event.text;
  console.log(text);
  text=text.split(' ');
  if(text[0]=='discuss'){
    
var discussion_name=text[1];
var participants=[];
con.query('SELECT token from userToken where userId=?', userId, function(err,res1){
    if(!err)
    {
      if(res1.length>0)
      {
      var token=res1['0'].token;
      console.log(token);
      flock.callMethod('groups.getMembers',token,{groupId:to},function(error,response){
       if(!error)
        {
          participants=response;
        async.map(participants,getColumns,function(error,results){
          if(!error) {
            participants=results;
            var requestData={discussion_name:discussion_name,userId:userId,username:username,participants:JSON.stringify(participants)}
   request({
    url:'https://d95df81a.ngrok.io/newDiscussion',
    method:'POST',
    json:requestData,
   },function(error,response,body){
    if(!error) {
      if(body=='done')
      {
          flock.callMethod('chat.sendMessage',config.botToken,{
      to:userId,
      attachments:[{
        title: "Discussion",
        description: "",
        views: {
          flockml:'<flockml> Discussion : '+discussion_name+' was created successfully</flockml>'
          },
        buttons:[{
         name:'View',
         action:{type:'openWidget',desktopType:'sidebar',mobileType:'sidebar',url:'https://d95df81a.ngrok.io/eventBar'},
        id:'view',
            }],
      }]
    },function(error,response){
      if(error) console.log(error);
      else console.log(response);
    });
      }
    }
    else console.log(error);
   })   
  } 

  });


        }

        
      
    }); 
    }
    
}
});
}
});
